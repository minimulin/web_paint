<?php
namespace Paint\BxORM;

use Bitrix\Main;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Entity\StringField;
use Paint\Interfaces\ProviderInterface;

/**
 * Класс модели для работы с записями об изображениях на основе ORM Bitrix
 * Какая же всё-таки эта ORM'ка глючная. На деле я предпочитаю использовать что-то отличное от ORM и даже инфоблоков, но мы ограничены заданием
 */
class PictureTable extends Main\Entity\DataManager implements ProviderInterface
{
    /**
     * Возвращает наименование таблицы в БД
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'paint_picture';
    }

    /**
     * Маппинг полей
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'id' => new IntegerField('id', [
                'primary' => true,
                'autocomplete' => true,
                'title' => 'Идентификатор',
            ]),
            'password' => new StringField('password', [
                'required' => true,
                'validation' => array(__CLASS__, 'validatePassword'),
                'title' => 'Пароль',
            ]),
            'file_id' => new IntegerField('file_id', [
                'required' => true,
                'title' => 'Идентификатор файла',
            ]),
        );
    }
    /**
     * Возвращает валидаторы для полей
     *
     * @return array
     */
    public static function validatePassword()
    {
        return array(
            new Main\Entity\Validator\Length(3, 255),
        );
    }

    /**
     * Возвращает запись по его идентификатору
     * @param  int $id Идентификатор
     * @return array   Данные о записи
     */
    public static function findOneById($id)
    {
        return PictureTable::getById($id)->fetch();
    }

    /**
     * Выполняет удалени записи
     * @param  int $id Идентификатор
     * @return bool    Результат удаления
     */
    public static function remove($id)
    {
        $result = PictureTable::delete($id);
        //Вернёт true даже если записи в БД нет.
        return $result->isSuccess();
    }

    /**
     * Возвращает все записи
     * @return array Массив записей
     */
    public static function findAll()
    {
        $sort = [
            'id' => 'asc',
        ];
        $select = array_keys(self::getMap());

        $result = PictureTable::getList([
            'select' => $select,
            'order' => $sort,
        ]);

        return $result->fetchAll();
    }

    /**
     * Выполняет сохранение записи
     * @param  int   $id   Идентификатор
     * @param  array $data Данные
     * @return bool        Результат сохранения
     */
    public static function saveById($id, $data)
    {
        $result = PictureTable::update($id, $data);
        return $result->isSuccess();
    }

    /**
     * Выполняет создание записи
     * @param  array $data Данные
     * @return bool        Результат создания
     */
    public static function create($data)
    {
        $result = PictureTable::add($data);
        return $result->isSuccess();
    }
}
