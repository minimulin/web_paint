<?php
namespace Paint\BxORM;

use Bitrix\Main\FileTable;
use Paint\Interfaces\Base64SaverInterface;
use Paint\Interfaces\ProviderInterface;
use \CFile;

/**
 * Класс для работы с файлами изображений средствами Битрикса
 * Основной класс — CFile
 */
class ImageTable extends CFile implements ProviderInterface, Base64SaverInterface
{

    /**
     * Ширина превью
     */
    const RESIZE_WIDTH = 85;

    /**
     * Высота превью
     */
    const RESIZE_HEIGHT = 64;

    /**
     * Возвращет изображение по его идентификатору
     * @param  int          $id Идентификатор
     * @return bool|array   Результат выборки
     */
    public static function findOneById($id)
    {
        $fileData = CFile::GetFileArray($id);
        //Выполняем ресайз изображения для создания превью
        $resizedFileData = CFile::ResizeImageGet($id, array('width' => self::RESIZE_WIDTH, 'height' => self::RESIZE_HEIGHT), BX_RESIZE_IMAGE_PROPORTIONAL, true);

        if ($fileData && $resizedFileData) {
            return [
                'id' => $fileData['ID'],
                'name' => $fileData['ORIGINAL_NAME'],
                'src' => $fileData['SRC'],
                'resized_src' => $resizedFileData['src'],
            ];
        }

        return false;
    }

    /**
     * Возвращает все изображения
     * @return array Массив с результатами выборки всех изображений
     */
    public static function findAll()
    {
        $sort = [
            'ID' => 'asc',
        ];

        $result = FileTable::getList([
            'select' => ['ID'],
            'order' => $sort,
        ]);

        $data = [];

        foreach ($result->fetchAll() as $key => $image) {
            $data[] = self::findOneById($image['ID']);
        }

        return $data;
    }

    /**
     * Сохранение файла изображения по его идентификатору
     * @param  int   $id   Идентификатор изображения
     * @param  array $data Данные по файлу изображения
     * @return bool        Результат сохранения
     */
    public static function saveById($id, $data)
    {
        $result = FileTable::update($id, $data);
        return $result->isSuccess();
    }

    /**
     * Создание записи об изображении
     * @param  array $data Данные
     * @return bool        Результат
     */
    public static function create($data)
    {
        $result = FileTable::add($data);
        return $result->isSuccess();
    }

    /**
     * Выполняет удаление изображения
     * @param  int $id Идентификатор
     * @return bool    Результат удаления
     */
    public static function remove($id)
    {
        return CFile::Delete($id);
    }

    /**
     * Выполняет создание изображения из формата Base64.
     */
    public static function createFromBase64String($base64, $id = null)
    {
        $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $base64));

        $image = [
            'type' => 'image/png',
            'content' => $data,
            'name' => md5($data) . '.png',
            'MODULE_ID' => 'paint',
        ];

        if (!is_null($id)) {
            $image['old_id'] = $id;
        }

        $fileId = CFile::SaveFile($image, "paint");

        return self::findOneById($fileId);
    }
}
