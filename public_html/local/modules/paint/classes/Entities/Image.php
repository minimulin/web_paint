<?php
namespace Paint\Entities;

/**
 * Сущность файла изображения
 */
class Image
{
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function getId()
    {
        return $this->data['id'];
    }

    public function setId($id)
    {
        $this->data['id'] = $id;
    }

    public function getName()
    {
        return $this->data['name'];
    }

    public function setName($name)
    {
        $this->data['name'] = $name;
    }

    public function setSrc($src)
    {
        $this->data['src'] = $src;
    }

    public function getSrc()
    {
        return $this->data['src'];
    }

    public function getResizedSrc()
    {
        return $this->data['resized_src'];
    }

    public function setResizedSrc($resizedSrc)
    {
        $this->data['resized_src'] = $resizedSrc;
    }

    public function toArray()
    {
        return $this->data;
    }
}
