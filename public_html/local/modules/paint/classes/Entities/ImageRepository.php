<?php
namespace Paint\Entities;

use Bitrix\Main\SystemException;
use Paint\Entities\Image;
use Paint\Interfaces\Base64SaverInterface;
use Paint\Interfaces\ImageRepositoryInterface;
use Paint\Util;

/**
 * Класс для работы с файлами изображений.
 * Идея была в том, чтобы в случае необходимости отойти от использования Битриксовых файлов и перейти на какое-то иное хранилище в случае необходимости.
 * Для того, чтобы перейти на новое хранилище, нужно будет просто реализовать класс-провайдер и подключить его здесь в свойстве $provider
 */
class ImageRepository implements ImageRepositoryInterface, Base64SaverInterface
{
    protected static $provider = 'Paint\\BxORM\\ImageTable';

    /**
     * Максимально допустимый размер загружаемого изображения
     */
    const MAX_FILE_SIZE = 10000000; //10 Mb

    /**
     * Поиск файла изображения по его идентификатору
     */
    public static function findOneById($id)
    {
        $provider = self::$provider;
        $result = $provider::findOneById($id);

        if (is_array($result)) {
            return new Image($result);
        }

        return false;
    }

    /**
     * Возвращает массив всех файлов изображений
     */
    public static function findAll()
    {
        $provider = self::$provider;
        $result = $provider::findAll();
        foreach ($result as $key => $picture) {
            $result[$key] = new Image($picture);
        }
        return $result;
    }

    /**
     * Обновляет файл изображения
     * @param  Image $entity Объект сущности
     */
    public static function update(Image $entity)
    {
        $provider = self::$provider;
        $data = $entity->toArray();

        if (isset($data['id'])) {
            Util::clearTagCache();

            return $provider::saveById($data['id'], $data);
        } else {
            throw new SystemException('Невозможно сохранить изображение без идентификатора', 1);
        }

        return false;
    }

    /**
     * Создаёт файл изображения
     * @param  Image $entity Объект сущности
     */
    public static function create(Image $entity)
    {
        $provider = self::$provider;
        $data = $entity->toArray();
        $result = $provider::create($data);

        Util::clearTagCache();

        return $result;
    }

    /**
     * Выполняет удаление файла изображения
     */
    public static function delete($id)
    {
        $provider = self::$provider;

        //Всегда возвращает null, к сожалению. Битрикс
        $result = $provider::delete($id);

        Util::clearTagCache();

        return $result;
    }

    /**
     * Выполняет валидацию изображения по Base64 строке
     * @param  string $base64String Изоражение в формате Base64
     * @return [type]               [description]
     */
    protected static function validateBase64($base64String)
    {
        $data = preg_replace('#^data:image/\w+;base64,#i', '', $base64String);

        if (base64_encode(base64_decode($data, true)) !== $data) {
            throw new SystemException('Невалидный формат изображения', 1);
        }

        $imgdata = base64_decode($data, true);
        if (strlen($imgdata) > self::MAX_FILE_SIZE) {
            throw new SystemException('Слишком большое изображение', 1);
        }

        return true;
    }

    /**
     * Создаёт изображение по Base64 строке
     * @param  string $base64String Изображение
     * @param  int $id              Идентификатор
     * @return mixed                Изображение
     */
    public static function createFromBase64String($base64String, $id = null)
    {
        $provider = self::$provider;

        try {
            self::validateBase64($base64String);
        } catch (Exception $e) {
            return false;
        }

        $result = $provider::createFromBase64String($base64String, $id);
        if (is_array($result)) {
            self::delete($id);
            return new Image($result);
        }

        return false;
    }
}
