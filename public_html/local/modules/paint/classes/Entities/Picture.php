<?php
namespace Paint\Entities;

use Paint\Entities\ImageRepository;

/**
 * Сущность изображения
 */
class Picture
{

    public function __construct($data)
    {
        $this->data = $data;
        $this->data['image'] = $this->getImage();
    }

    public function getId()
    {
        return $this->data['id'];
    }

    public function setId($id)
    {
        $this->data['id'] = $id;
    }

    public function getPassword()
    {
        return $this->data['password'];
    }

    public function setPassword($password)
    {
        $this->data['password'] = $password;
    }

    public function getFileId()
    {
        return $this->data['file_id'];
    }

    public function setFileId($fileId)
    {
        $this->data['file_id'] = $fileId;
    }

    public function getImage()
    {
        return ImageRepository::findOneById($this->getFileId());
    }

    public function setImage(Image $image)
    {
        $this->setFileId($image->getId());
    }

    public function getImageData()
    {
        return $this->data['image_data'];
    }

    public function setImageData($imageData)
    {
        $this->data['image_data'] = $imageData;
    }

    public function toArray()
    {
        //Нам нужны не все данные
        $resultData = $this->data;
        unset($resultData['image_data'], $resultData['image']);

        return $resultData;
    }
}
