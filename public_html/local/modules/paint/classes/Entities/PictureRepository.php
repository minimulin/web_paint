<?php
namespace Paint\Entities;

use Bitrix\Main\Localization\Loc;
use Paint\Entities\Picture;
use Paint\Entities\ImageRepository;
use Bitrix\Main\SystemException;
use Paint\Interfaces\PictureRepositoryInterface;
use Paint\Util;


/**
 * Класс для работы с записями изображений.
 * Идея была в том, чтобы в случае необходимости отойти от использования ORM Bitrix и перейти на другую ORM или ещё что-то
 */
class PictureRepository implements PictureRepositoryInterface
{
    protected static $provider = 'Paint\\BxORM\\PictureTable';

    /**
     * Возвращает массив всех изображений
     */
    public static function findAll()
    {
        $provider = self::$provider;
        $result = $provider::findAll();
        foreach ($result as $key => $picture) {
            $result[$key] = new Picture($picture);
        }
        return $result;
    }

    /**
     * Поиск изображения по его идентификатору
     */
    public static function findOneById($id) {
        $provider = self::$provider;
        $result = $provider::findOneById($id);

        if (is_array($result)) {
            return new Picture($result);
        }

        return false;
    }

    /**
     * Выполняет удаление изображения по его идентификатору
     */
    public static function delete($id) {
        $provider = self::$provider;

        $picture = self::findOneById($id);
        if ($picture) {
            ImageRepository::delete($picture->getFileId());
            $result = $provider::remove($id);
            Util::clearTagCache();
            return $result;
        }

        return false;
    }

    /**
     * Обновление изображения
     * @param  Picture $entity Объект сущности
     */
    public static function update(Picture $entity)
    {
        $provider = self::$provider;

        if ($entity->getFileId() && $entity->getImageData()) {
            $image = ImageRepository::createFromBase64String($entity->getImageData(), $entity->getFileId());

            if ($image) {
                $entity->setImage($image);
            } else {
                throw new SystemException('Невозможно сохранить изображение', 1);
            }
        }

        $data = $entity->toArray();

        if (isset($data['id'])) {
            $result = $provider::saveById($data['id'], $data);

            Util::clearTagCache();

            return $result;
        } else {
            throw new SystemException('Невозможно сохранить изображение без идентификатора', 1);
        }

        return false;
    }

    /**
     * Создаёт изображение
     * @param  Picture $entity Объект сущности
     */
    public static function create(Picture $entity)
    {
        $provider = self::$provider;

        if (!$entity->getFileId() && $entity->getImageData()) {
            $image = ImageRepository::createFromBase64String($entity->getImageData());
            if ($image) {
                $entity->setImage($image);
            } else {
                throw new SystemException('Невозможно сохранить изображение', 1);
            }
        }
        $data = $entity->toArray();

        $result = $provider::create($data);

        Util::clearTagCache();

        return $result;
    }
}
