<?php

namespace Paint\Interfaces;

/**
 * Интерфейс, определяющий возможность сохранения изображения из строки формата Base64
 */
interface Base64SaverInterface
{

	/**
	 * Метод, который необходимо реализовать для создания изображения из Base64 строки
	 * @param  string $base64String Данные изображения
	 */
    public static function createFromBase64String($base64String);

}
