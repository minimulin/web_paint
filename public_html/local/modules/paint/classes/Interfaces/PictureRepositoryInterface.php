<?php

namespace Paint\Interfaces;

use Paint\Entities\Picture;

/**
 * Задел на то, чтобы отойти от Bitrix ORM, т.к. есть вещи и получше
 * Но поскольку мы ограничены заданием, то будем использовать его
 */
interface PictureRepositoryInterface
{

    public static function findAll();
    public static function findOneById($id);
    public static function update(Picture $entity);
    public static function create(Picture $entity);
    public static function delete($id);

}
