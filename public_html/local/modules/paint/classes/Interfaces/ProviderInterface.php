<?php

namespace Paint\Interfaces;

/**
 * Задел на то, чтобы отойти от Bitrix ORM, т.к. есть вещи и получше
 * Но поскольку мы ограничены заданием, то будем использовать его
 */
interface ProviderInterface
{

    public static function findAll();
    public static function findOneById($id);
    public static function saveById($id, $data);
    public static function create($data);
    public static function remove($id);

}
