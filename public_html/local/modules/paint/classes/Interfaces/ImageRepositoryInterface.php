<?php

namespace Paint\Interfaces;

use Paint\Entities\Image;

/**
 * Задел на то, чтобы отойти от Bitrix ORM, т.к. есть вещи и получше
 * Но поскольку мы ограничены заданием, то будем использовать его
 */
interface ImageRepositoryInterface
{

    public static function findAll();
    public static function findOneById($id);
    public static function update(Image $entity);
    public static function create(Image $entity);
    public static function delete($id);

}
