<?php

namespace Paint;

use Bitrix\Main\Application;
use \Exception;

/**
* Полезные методы
*/
class Util
{
	
	/**
	 * Возвращает является ли запрос AJAX'ом
	 * @return boolean Флаг AJAX-запроса
	 */
	public static function isAjax()
	{
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			return true;
		}
		return false;
	}

	/**
	 * Вычисляет hash-функцию от пароля. Используется BCRYPT
	 * @param  string $pass Пароль
	 * @return string       Хэш пароля
	 */
	public static function getPassHash($pass)
	{
		return password_hash($pass, PASSWORD_BCRYPT);
	}

	/**
	 * Выполняет валидацию пароля и хэша
	 * @param  string $pass Пароль
	 * @param  string $hash Хэш
	 * @return bool         Результат проверки
	 */
	public function verifyPasswordHash($pass, $hash)
	{
		return password_verify($pass, $hash);
	}

	/**
	 * Возвращает пароль из сессии
	 * @param  int $pictureId Идентификатор изображения
	 * @return string         Пароль из сессии
	 */
	public function getPictureHashFromSession($pictureId)
	{
		if (isset($_SESSION['picturePassHashes'][$pictureId])) {
			return $_SESSION['picturePassHashes'][$pictureId];
		} 

		return null;
	}

	/**
	 * Сохраняет валидные пароль к изображению
	 * @param int    $pictureId Идентификатор изображения
	 * @param string $hash      Хэш
	 */
	public static function setPictureHashToSession($pictureId, $hash)
	{
		if (is_null($hash)) {
			unset($_SESSION['picturePassHashes'][$pictureId]);
		} else {
			$_SESSION['picturePassHashes'][$pictureId] = $hash;	
		}	
	}

	/**
	 * Выполняет валидацию пароля, введённого пользователем на форме добавления изображения
	 * @param  string      $pass Пароль
	 * @return bool|string       Результат валидации
	 */
	public static function validatePass($pass)
	{
		if (strlen($pass) <= 3) {
			return 'Пароль должен содержать более 3 символов';
		}

		if (strlen($pass) >= 72) {
			//Ограничение bcrypt
			return 'Пароль должен содержать менее 72 символов';
		}

		return true;
	}

	/**
	 * Выполняет очистку тегового кеша для списка изображений
	 */
	public static function clearTagCache()
    {
        $cacheManager = Application::getInstance()->getTaggedCache();
        $cacheManager->clearByTag('list');
    }
}