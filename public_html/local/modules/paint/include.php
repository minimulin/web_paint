<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

$moduleDir = dirname(__FILE__);

// Автоматически файлы автолоадятся, если имя модуля идёт с именем партнёра. Типа pupkin.paint
// Решил отойти от данного механизма, поскольку писать каждый класс просто-напросто некрасиво. И поэтому избавился от каталога lib.
// Bitrix\Main\Loader::registerAutoLoadClasses(null, [
//     '\Paint\Picture' => '/local/modules/paint/lib/Picture.php',
//     '\Paint\PictureInterface' => '/local/modules/paint/lib/PictureInterface.php',
// ]);

// Такой способ мне больше нравится
spl_autoload_register(function ($class) {
    $prefix = 'Paint';
    $baseDir = __DIR__ . '/classes';
    $len = strlen($prefix);

    if (strncmp($prefix, $class, $len) !== 0) {
        return;
    }

    $relativeClass = substr($class, $len);

    $file = $baseDir.str_replace('\\', '/', $relativeClass).'.php';

    if (file_exists($file)) {
        require_once $file;
    }
});
