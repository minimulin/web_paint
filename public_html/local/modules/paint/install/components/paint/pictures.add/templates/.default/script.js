$(function() {
	$('#canvas').sketch();

	$('#addForm').on('submit', function(event){
		$('#sendAdd').prop('disabled',true);
		$.ajax({
			type: "POST",
			url: "?",
			dataType: 'json',
			data: { 
				sessid: $('[name="sessid"]').val(),
				password: $('[name="password"]').val(),
				imageData: $('#canvas').sketch('getImage')
			}
		}).done(function(data) {
			if (typeof(data.success) != 'undefined') {
				if (data.success) {
					window.location = '/paint/';
				} else {
					$('#sendAdd').removeProp('disabled');
					if (typeof(data.error) != 'undefined') {
						alert(data.error);
					}
				}
			}
		});

		event.preventDefault();
	});
});