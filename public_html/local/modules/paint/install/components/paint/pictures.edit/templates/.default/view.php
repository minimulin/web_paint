<div class="container">
	<h2 class="text-center">Просмотр изображения</h2>
	<hr>

	<div class="text-center">
		<img class="bordered" src="<?= $arResult['IMAGE']->getSrc(); ?>">
	</div>

	<hr>

	<a class="btn btn-primary" href="/paint/edit/<?= $arResult['PICTURE']->getId(); ?>" class="btn">Редактировать</a>

</div>