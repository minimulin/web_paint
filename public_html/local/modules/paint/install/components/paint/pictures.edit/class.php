<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\SystemException;
use Paint\Entities\PictureRepository;
use Paint\Util;

class PicturesEditComponent extends CBitrixComponent
{
    /**
     * Подключение языковых файлов
     */
    public function onIncludeComponentLang()
    {
        $this->includeComponentLang(basename(__FILE__));
        Loc::loadMessages(__FILE__);
    }

    /**
     * Подготовка и проверка параметров компонента
     * @param  array $params Входящие параметры компонента
     * @return array         Подготовленные параметры компонента
     */
    public function onPrepareComponentParams($params)
    {
        if (!isset($params['PICTURE_ID'])) {
            throw new SystemException('Не задан параметр PICTURE_ID', 1);
        }

        $result = array(
            'VIEW' => ($params['VIEW'] == 'Y' ? 'Y' : 'N'),
            'PICTURE_ID' => $params['PICTURE_ID'],
        );

        return $result;
    }

    /**
     * Выполняет получение данных из репозитория
     * @return bool Флаг успешности получения результата
     */
    protected function getResult()
    {
        $picture = PictureRepository::findOneById($this->arParams['PICTURE_ID']);
        if (!$picture) {
            $this->AbortResultCache();
            @define('ERROR_404', 'Y');
            return false;
        } else {
            $this->arResult = [
                'PICTURE' => $picture,
                'IMAGE' => $picture->getImage(),
            ];

            return true;
        }
    }

    /**
     * Выполняет проверку прав на доступ к редактированию изображения
     * @return bool Флаг прав на редактирование
     */
    public function checkPermission()
    {
        $request = Application::getInstance()->getContext()->getRequest();
        $password = $request->getPost('password');

        $picturePassword = $this->arResult['PICTURE']->getPassword();
        $pictureId = $this->arResult['PICTURE']->getId();

        //Если пароль не пришёл с формы
        if (is_null($password)) {
            $password = Paint\Util::getPictureHashFromSession($pictureId);
            // Проверяем совпадение паролей у изображения и в сессии
            if (Util::verifyPasswordHash($password, $picturePassword)) {
                return true;
            }
        }
        //Если пароль пришёл
        else {
            // Проверяем правильность переданного пароля
            if (Util::verifyPasswordHash($password, $picturePassword)) {
                //Сохраняем пароль в сессию
                Paint\Util::setPictureHashToSession($pictureId, $password);
                return true;
            }
        }

        return false;
    }

    public function validateRequest()
    {
        if (!check_bitrix_sessid()) {
            echo json_encode([
                'success' => false,
                'error' => 'Возникла ошибка. Перезагрузите страницу',
            ]);
            die();
        }

        $request = Application::getInstance()->getContext()->getRequest();

        if (!is_null($request->getPost('password'))) {
            if (!$this->checkPermission()) {
                echo json_encode([
                    'success' => false,
                    'error' => 'Неверный пароль',
                ]);
                die();
            } else {
                echo json_encode([
                    'success' => true,
                ]);
                die();
            }
        }
    }

    /**
     * Логика компонента
     */
    public function executeComponent()
    {
        if (!$this->getResult()) {
            return;
        }

        // Шаблон просмотра изображения
        if (isset($this->arParams['VIEW']) && $this->arParams['VIEW'] == 'Y') {
            $this->includeComponentTemplate('view');
            return;
        }

        if (Util::isAjax()) {
            global $APPLICATION;
            $APPLICATION->RestartBuffer();

            $request = Application::getInstance()->getContext()->getRequest();

            $this->validateRequest();

            // Изображение в base64
            $imageData = $request->getPost('imageData');

            $picture = $this->arResult['PICTURE'];
            $picture->setImageData($imageData);

            $result = PictureRepository::update($picture);

            if (!$result) {
                throw new SystemException('Не удалось сохранить файл', 1);
            }

            // После успешного редактирования сбрасываем пароль из сессии
            Paint\Util::setPictureHashToSession($picture->getId(), null);

            echo json_encode([
                'success' => true,
            ]);
            die();
        } else {
            if (!$this->checkPermission()) {
                $this->includeComponentTemplate('pass');
                return;
            }
        }

        $this->includeComponentTemplate('edit');
    }
}
