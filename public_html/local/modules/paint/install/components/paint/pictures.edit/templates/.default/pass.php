<div class="container">
	<h2>Введите пароль</h2>

	<form id="passForm" method="post" action="?">
		<?=bitrix_sessid_post()?>
		<div class="row">
			<div class="col-lg-10">
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon1">*</span>
					<input type="password" name="password" class="form-control" placeholder="Пароль" aria-describedby="sizing-addon1">
				</div>
			</div><!-- /.col-lg-6 -->
			<div class="col-lg-2">
				<div class="input-group">
					<button id="sendPass" type="submit" class="btn btn-primary">Отправить</button>
				</div>
			</div><!-- /.col-lg-6 -->
		</div><!-- /.row -->
	</form>


</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#passForm').on('submit', function (event) {
			$('#sendPass').prop('disabled',true);
			$.ajax({
				type: "POST",
				url: "?",
				dataType: 'json',
				data: { 
					sessid: $('[name="sessid"]').val(),
					password: $('[name="password"]').val()
				}
			}).done(function(data) {
				if (typeof(data.success) != 'undefined') {
					if (data.success) {
						location.reload();
					} else {
						$('#sendPass').removeProp('disabled');
						if (typeof(data.error) != 'undefined') {
							alert(data.error);
						}
					}
				}
			});

			event.preventDefault();
		});
	});
</script>