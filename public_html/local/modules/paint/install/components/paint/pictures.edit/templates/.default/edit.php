<div class="container">
	<h2 class="text-center">Редактирование изображения</h2>
	<hr>

	<div class="text-center">
		<canvas class="bordered" id="canvas" width="800" height="600" data-image="<?= $arResult['IMAGE']->getSrc(); ?>"></canvas>	
	</div>

	<hr>

	<form id="editForm" method="post" action="?">
		<?=bitrix_sessid_post()?>
		<div class="row">

			<div class="col-lg-2">
				<div class="input-group">
					<button id="sendEdit" type="submit" class="btn btn-primary">Сохранить</button>
				</div>
			</div><!-- /.col-lg-6 -->
		</div><!-- /.row -->
	</form>
	
</div>

<?/* Вынес из всех файлов отдельно, чтобы не попадало в кешированные файлы */?>
<script type="text/javascript">
	$('#canvas').sketch();

	$('#editForm').on('submit', function(event){
		$('#sendEdit').prop('disabled',true);
		$.ajax({
			type: "POST",
			url: "?",
			dataType: 'json',
			data: {
				sessid: $('[name="sessid"]').val(),
				imageData: $('#canvas').sketch('getImage')
			}
		}).done(function(data) {
			if (typeof(data.success) != 'undefined') {
				if (data.success) {
					window.location = '/paint/';
				} else {
					$('#sendEdit').removeProp('disabled');
					if (typeof(data.error) != 'undefined') {
						alert(data.error);
					}
				}
			}
		});

		event.preventDefault();
	});
</script>