<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Localization\Loc;
use Paint\Entities\PictureRepository;

class PicturesListComponent extends CBitrixComponent
{
    /**
     * Параметры постраничной навигации
     * @var array
     */
    protected $navParams = [];

    /**
     * Подключение языковых файлов
     */
    public function onIncludeComponentLang()
    {
        $this->includeComponentLang(basename(__FILE__));
        Loc::loadMessages(__FILE__);
    }

    /**
     * Подготовка входных параметров
     * @param  array $params Входные параметры
     * @return array         Подготовленные параметры
     */
    public function onPrepareComponentParams($params)
    {
        $result = array(
            'SHOW_NAV' => ($params['SHOW_NAV'] == 'Y' ? 'Y' : 'N'),
            'COUNT' => intval($params['COUNT']),
            'CACHE_TIME' => intval($params['CACHE_TIME']) > 0 ? intval($params['CACHE_TIME']) : 3600,
        );
        return $result;
    }

    /**
     * Подготовка к пагинации
     */
    protected function executeProlog()
    {
        if ($this->arParams['COUNT'] > 0) {
            if ($this->arParams['SHOW_NAV'] == 'Y') {
                \CPageOption::SetOptionString('main', 'nav_page_in_session', 'N');
                $this->navParams = array(
                    'nPageSize' => $this->arParams['COUNT'],
                );
                $arNavigation = \CDBResult::GetNavParams($this->navParams);
                $this->cacheAddon = array($arNavigation);
            } else {
                $this->navParams = array(
                    'nTopCount' => $this->arParams['COUNT'],
                );
            }
        } else {
            $this->navParams = false;
        }

    }

    /**
     * Основная логика компонента
     */
    public function executeComponent()
    {
        $this->executeProlog();

        $cache_id = md5(serialize($this->arParams));
        $cache_dir = "/pictures";
        
        $сache = Bitrix\Main\Data\Cache::createInstance();
        $cacheManager = Bitrix\Main\Application::getInstance()->getTaggedCache();

        try {
            if ($сache->initCache($this->arParams['CACHE_TIME'], $cache_id, $cache_dir)) {
                $this->arResult['ITEMS'] = $сache->getVars();
            } elseif ($сache->startDataCache()) {
                $cacheManager->startTagCache($cache_dir);

                $this->arResult['ITEMS'] = PictureRepository::findAll();

                //По данному тегу будем сбрасывать кеш при изменении в БД
                $cacheManager->registerTag('list');
                $cacheManager->endTagCache();

                $сache->endDataCache($this->arResult['ITEMS']);
            } else {
                $this->arResult['ITEMS'] = [];
            }

            $this->includeComponentTemplate();
        } catch (Exception $e) {
            $сache->abortDataCache();
            $cacheManager->clearByTag('tag');
            ShowError($e->getMessage());
        }
    }
}
