<div class="container">
	<h2 class="text-center">Список изображений</h2>
	<hr>
	<p>Найдено изображений: <?= count($arResult['ITEMS']); ?></p>

	<?php foreach ($arResult['ITEMS'] as $picture): ?>
		<?php $image = $picture->getImage(); ?>
		<?php if ($image): ?>
			<img src="<?= $image->getResizedSrc() ?>">
		<?php endif; ?>
		<a class="btn btn-primary" href="/paint/view/<?= $picture->getId(); ?>">Просмотр</a>
		<a class="btn btn-primary" href="/paint/edit/<?= $picture->getId(); ?>">Редактирование</a>
		<hr>
	<?php endforeach; ?>
</div>