<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>

<?php

$APPLICATION->IncludeComponent(
    'paint:pictures.list',
    '',
    [
        'CACHE_TIME' => $arParams['CACHE_TIME'],
        'CACHE_TYPE' => $arParams['CACHE_TYPE'],
        'COUNT' => $arParams['COUNT'],
        'SHOW_NAV' => $arParams['SHOW_NAV'],
    ],
    $component
);?>
