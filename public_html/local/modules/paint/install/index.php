<?php

use Bitrix\Main\Application;

class paint extends CModule
{
    public $MODULE_ID = "paint";
    public $MODULE_VERSION = "1.0.0";
    public $MODULE_VERSION_DATE = "2016-12-23 00:00:00";
    public $MODULE_NAME = "Paint";
    public $MODULE_DESCRIPTION = "Paint";

    public function paint()
    {
        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include $path . "/version.php";
    }
    public function DoInstall()
    {
        global $DOCUMENT_ROOT, $APPLICATION;
        $this->InstallFiles();
        $this->InstallDB();
        RegisterModule($this->MODULE_ID);
        $APPLICATION->IncludeAdminFile('Установка модуля ' . $this->MODULE_ID, $DOCUMENT_ROOT . "/local/modules/".$this->MODULE_ID."/install/step.php");
    }

    public function DoUninstall()
    {
        global $DOCUMENT_ROOT, $APPLICATION;
        $this->UnInstallFiles();
        $this->UnInstallDB();
        UnRegisterModule($this->MODULE_ID);
        $APPLICATION->IncludeAdminFile('Деинсталляция модуля ' .  $this->MODULE_ID, $DOCUMENT_ROOT . "/local/modules/".$this->MODULE_ID."/install/unstep.php");
    }

    public function InstallDB($arParams = array()) {
        global $DB;
        $result = $DB->RunSQLBatch(Application::getDocumentRoot() . "/local/modules/".$this->MODULE_ID."/install/db/install.sql");
        
        return true;
    }

    public function UnInstallDB($arParams = array()) {
        global $DB;
        $result = $DB->RunSQLBatch(Application::getDocumentRoot().'/'."/local/modules/".$this->MODULE_ID."/install/db/uninstall.sql");
        return true;
    }

    public function InstallFiles($arParams = array())
    {
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/local/modules/".$this->MODULE_ID."/install/components",
            $_SERVER["DOCUMENT_ROOT"] . "/local/components", true, true);
        return true;
    }

    public function UnInstallFiles()
    {
        DeleteDirFilesEx($_SERVER["DOCUMENT_ROOT"] . '/local/components/".$this->MODULE_ID."/');
        return true;
    }

}
