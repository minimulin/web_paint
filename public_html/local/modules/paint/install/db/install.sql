CREATE TABLE IF NOT EXISTS `paint_picture` (
`id` int(10) unsigned NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 NOT NULL,
  `file_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE `paint_picture`
 ADD PRIMARY KEY (`id`);


ALTER TABLE `paint_picture`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;