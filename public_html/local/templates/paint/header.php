<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Page\Asset;

Loc::loadMessages(__FILE__);

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title><?=$APPLICATION->ShowTitle();?></title>

		<?php Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/css/bootstrap.min.css', true);?>
		<?php Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/css/style.css', true);?>
		<? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/jquery.min.js', true); ?>
		<? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/bootstrap.min.js', true); ?>
		<? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH  . '/js/sketch.js', true); ?>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<?php $APPLICATION->ShowHead();?>
		<title><?php $APPLICATION->ShowTitle();?></title>
	</head>
	<body>
		<?php $APPLICATION->ShowPanel();?>

		<nav class="navbar navbar-inverse">
			<div class="container">
				<div class="navbar-header">
				 	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only"><?=Loc::getMessage("NAVIGATION");?></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/">Paint</a>
				</div>
				<div id="navbar" class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
					<li><a href="/paint/">Список</a></li>
						<li><a href="/paint/add/">Добавить</a></li>
					</ul>
				</div><!--/.nav-collapse -->
			</div>
		</nav>