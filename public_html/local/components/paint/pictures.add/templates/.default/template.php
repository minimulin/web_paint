<div class="container">
	<h2 class="text-center">Добавление нового изображения</h2>
	<hr>

	<div class="text-center">
		<canvas class="bordered" id="canvas" width="800" height="600"></canvas>	
	</div>

	<hr>

	<form id="addForm" method="post" action="?">
		<?=bitrix_sessid_post()?>
		<div class="row">
			<div class="col-lg-10">
				<div class="input-group">
					<span class="input-group-addon" id="sizing-addon1">*</span>
					<input type="password" name="password" class="form-control" placeholder="Пароль" aria-describedby="sizing-addon1">
				</div>
			</div><!-- /.col-lg-6 -->
			<div class="col-lg-2">
				<div class="input-group">
					<button id="sendAdd" type="submit" class="btn btn-primary">Отправить</button>
				</div>
			</div><!-- /.col-lg-6 -->
		</div><!-- /.row -->
	</form>
</div>