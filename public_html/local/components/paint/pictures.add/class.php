<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\SystemException;
use Paint\Entities\Picture;
use Paint\Entities\PictureRepository;
use Paint\Util;

/**
 * Компонент добавления изображения
 */
class PicturesAddComponent extends CBitrixComponent
{

    /**
     * Логика компонента
     */
    public function executeComponent()
    {
        if (Util::isAjax()) {
            global $APPLICATION;
            $APPLICATION->RestartBuffer();

            $request = Application::getInstance()->getContext()->getRequest();
            $imageData = $request->getPost('imageData');
            $password = $request->getPost('password');

            //Проверка сессии и пароля
            $this->validateRequest();

            $picture = new Paint\Entities\Picture([
                'password' => Util::getPassHash($password),
                // Изображение в base64
                'image_data' => $imageData,
            ]);

            $result = PictureRepository::create($picture);

            if (!$result) {
                throw new SystemException('Не удалось сохранить файл', 1);
            } else {
                echo json_encode([
                    'success' => true,
                ]);
                exit;
            }
        } else {
            $this->includeComponentTemplate();
        }
    }

    /**
     * Выполняем валидацию запроса
     */
    public function validateRequest()
    {
        if (!check_bitrix_sessid()) {
            echo json_encode([
                'success' => false,
                'error' => 'Возникла ошибка. Перезагрузите страницу',
            ]);
            exit;
        }

        $request = Application::getInstance()->getContext()->getRequest();
        $password = $request->getPost('password');

        $validationResult = Util::validatePass($password);
        if ($validationResult !== true) {
            echo json_encode([
                'success' => false,
                'error' => $validationResult,
            ]);
            exit;
        };
    }

    /**
     * Подключаем языковые файлы
     */
    public function onIncludeComponentLang()
    {
        $this->includeComponentLang(basename(__FILE__));
        Loc::loadMessages(__FILE__);
    }

}
