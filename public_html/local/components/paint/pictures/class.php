<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use \CBitrixComponent;
use \CComponentEngine;

/**
 * Общий комплексный компонент-роутер
 */
class PicturesComponent extends CBitrixComponent
{

    protected $defaultUrlTemplates = [
        'list' => '',
        'add' => 'add/',
        'view' => 'view/#ELEMENT_ID#',
        'edit' => 'edit/#ELEMENT_ID#',
    ];

    protected $componentVariables = [
        'ELEMENT_ID',
    ];

    /**
     * Выполняем компонент
     */
    public function executeComponent()
    {
        $template = $this->handleUrl();
        $this->getResult();
        $this->includeComponentTemplate($template);
    }

    /**
     * Возвращает строку с шаблоном, который необходимо подключить
     * @return string Строка с именем шаблона
     */
    protected function handleUrl()
    {
        $this->urlTemplates = [];

        if ($this->arParams['SEF_MODE'] == 'Y') {
            $this->variables = [];

            // Объединяем дефолтные шаблоны с теми, что могут прийти в параметрах
            $this->urlTemplates = CComponentEngine::MakeComponentUrlTemplates(
                $this->defaultUrlTemplates,
                $this->arParams['URL_TEMPLATES']
            );

            // Получим массив псевдонимов переменных
            $this->variableAliases = CComponentEngine::MakeComponentVariableAliases(
                $this->defaultUrlTemplates,
                $this->arParams['VARIABLE_ALIASES']
            );

            //Определяем по URL нужный нам шаблон
            $engine = new CComponentEngine($this);
            $template = $engine->guessComponentPath(
                $this->arParams['SEF_FOLDER'],
                $this->urlTemplates,
                $this->variables
            );

            if (empty($template)) {
                $template = 'list';
            }

            CComponentEngine::InitComponentVariables(
                $template,
                $this->componentVariables,
                $this->variableAliases,
                $this->variables
            );
        } else {
            $template = 'list';
        }

        return $template;
    }

    /**
     * Задаёт результирующий массив данных
     */
    protected function getResult()
    {
        $this->arResult = array(
            'FOLDER' => $this->arParams['SEF_FOLDER'],
            'URL_TEMPLATES' => $this->urlTemplates,
            'VARIABLES' => $this->variables,
            'ALIASES' => $this->variableAliases,
        );
    }
}
