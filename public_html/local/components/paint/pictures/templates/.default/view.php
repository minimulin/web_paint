<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>

<?php

$APPLICATION->IncludeComponent(
    'paint:pictures.edit',
    '',
    [
        'VIEW' => 'Y',
        'PICTURE_ID' => $arResult['VARIABLES']['ELEMENT_ID'],
    ],
    $component
);?>
