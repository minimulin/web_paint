<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>

<?php

$APPLICATION->IncludeComponent(
    'paint:pictures.edit',
    '',
    [
        'PICTURE_ID' => $arResult['VARIABLES']['ELEMENT_ID'],
    ],
    $component
);?>
